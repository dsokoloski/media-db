#!/bin/sh

find $(pwd) -name configure.ac | xargs touch

mkdir -vp m4

# Regenerate configuration files
autoreconf -i --force || exit 1

