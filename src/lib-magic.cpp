#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <map>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include <iostream>

using namespace std;

#include <media-db/magic.h>
#include <media-db/util.h>

libMagic::libMagic(const char *magic_db, const char *mime_file, int flags)
    : mdb(NULL)
{
    mdb = magic_open(flags);
    if (!mdb) {
        libOutput(O_ERROR, "magic_open: %s\n", magic_db);
        return;
    }

    if (magic_load(mdb, magic_db) != 0) {
        libOutput(O_ERROR, "magic_load: %s: %s\n", magic_db, magic_error(mdb));
        magic_close(mdb);
        mdb = NULL;
    }

    int page_size = ::libUtil_getpagesize();
    char buffer[page_size];

	FILE *h = fopen(mime_file, "r");

	if (!h) {
        libOutput(O_ERROR, "fopen: %s: %s\n", mime_file, strerror(errno));
        return;
    }

	for ( ;; ) {
		int i;
		string mime_type;

		if (fgets(buffer, page_size, h) == NULL) break;

		for (i = 0; i < page_size; i++) {
			if (!isspace(buffer[i])) break;
		}

		if (buffer[i] == '#' || !isprint(buffer[i])) continue;

		for ( ; i < page_size; i++) {
			if (isspace(buffer[i]) || !isprint(buffer[i])) break;
			mime_type.append(1, buffer[i]);
		}

		for ( ; i < page_size; i++) {
			if (!isspace(buffer[i])) break;
		}

		if (!isprint(buffer[i])) continue;

		do {
			string file_ext;

			for ( ; i < page_size; i++) {
				if (isspace(buffer[i]) || !isprint(buffer[i])) break;
				file_ext.append(1, buffer[i]);
			}

			for ( ; i < page_size; i++) {
				if (!isspace(buffer[i])) break;
			}

			if (!file_ext.size()) break;

            mime_types[file_ext] = mime_type;

		} while (i < page_size);
	}

	fclose(h);
}

libMagic::~libMagic()
{
    if (mdb != NULL) magic_close(mdb);
}

const char *libMagic::IdentifyByMagic(const char *file)
{
    return magic_file(mdb, file);
}

const char *libMagic::IdentifyByExtension(const char *file)
{
    const char *extension = NULL;
    const char *mime_type = "application/unknown";

    for (int i = strlen(file) - 1; i >= 0; i--) {
        if (file[i] != '.') continue;
        extension = &file[i + 1];
        break;
    }

    if (extension != NULL && strlen(extension)) {
        string ext;
        for (const char *p = extension; *p != '\0'; p++)
            ext.append(1, tolower(*p));
        libMagicMimeType::iterator i;
        i = mime_types.find(ext);
        if (i != mime_types.end())
            mime_type = i->second.c_str();
    }

    return mime_type;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
