#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <deque>
#include <string>
#include <stdexcept>
#include <sstream>

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>
#include <netdb.h>

#include <sys/stat.h>
#include <sys/socket.h>

#include <pthread.h>
#include <string.h>
#include <errno.h>

#if 0
#include <uriparser/Uri.h>
#define URI_RANGE(x)  (int)((x).afterLast-(x).first), ((x).first)
    UriUriA uri;
    UriParserStateA state;
    state.uri = &uri;

    if (uriParseUriA(&state, "http://google.ca:8182/test.php") != URI_SUCCESS) {
        libOutput(O_DEBUG, "Uriparser error\n");
        return 1;
    }
    libOutput(O_DEBUG, "scheme: %.*s\n", URI_RANGE(uri.scheme));
#endif

using namespace std;

#include <media-db/exception.h>
#include <media-db/magic.h>
#include <media-db/thread.h>
#include <media-db/import.h>
#include <media-db/util.h>
#include <media-db/event.h>
#include <media-db/main.h>
#include <media-db/socket.h>

#include "mdb-import.h"

mdbImport::mdbImport(int argc, char * const *argv)
    : libMain(argc, argv), magic_db(DEFAULT_MAGIC_DB), mime_file(DEFAULT_MIME_FILE),
    recursive(false)
{
    libMainOption *option;

    option = new libMainOption;
    option->long_opt = "magic-db";
    option->short_opt = 'm';
    option->has_arg = 1;
    option->arg_name = "database";
    option->help = "Specify alternate magic database.";
    option->default_value = magic_db;
    options.insert(options.begin(), option);

    option = new libMainOption;
    option->long_opt = "mime-file";
    option->short_opt = 'i';
    option->has_arg = 1;
    option->arg_name = "file";
    option->help = "Specify alternate MIME file.";
    option->default_value = mime_file;
    options.insert(options.begin(), option);

    option = new libMainOption;
    option->long_opt = "recursive";
    option->short_opt = 'R';
    option->has_arg = 0;
    option->help = "Perform a recursive path scan.";
    options.insert(options.begin(), option);
/*
    option = new libMainOption;
    option->long_opt = "test";
    option->short_opt = 't';
    option->has_arg = 1;
    option->arg_name = "value";
    option->help = "Test option.";
    option->default_value = "/etc/media-db.conf";
    options.insert(options.begin(), option);
*/
    prog_usage = "[<options>] <import path> [<import path>]...";
    prog_help = "  Specifiy one or more import paths after any options.";

    ParseArguments();

    libOutputContext ctx;
    ctx.type = libOutput_STDERR;
    ctx.syslog_ident = prog_name;
    ctx.debug = debug;

    libOutputFree();
    libOutputInit(ctx);

    if (optind == argc) {
        libOutput(O_WARN, "Required argument missing.\nTry --help for usage information.\n");
        exit(1);
    }

    try {
        libSocketClientLocal client("/tmp/test-socket");
        //libSocketClientRemote client("localhost", "5500");
    } catch (exception &e) {
        libOutput(O_ERROR, "Exception: %s\n", e.what());
//        exit(1);
    }
}

mdbImport::~mdbImport()
{
}

void mdbImport::SetOption(char option, const char *optarg)
{
    libMain::SetOption(option, optarg);

    libMainOption *o = NULL;
    for (vector<libMainOption *>::iterator i = options.begin();
        i != options.end(); i++) {
        if ((*i)->short_opt == option) {
            o = (*i);
            break;
        }
    }

    if (o == NULL) return;

    switch (option) {
    case 'm':
        magic_db = optarg;
        break;
    case 'i':
        mime_file = optarg;
        break;
    case 'R':
        recursive = true;
        break;
    }

    return;
}

int mdbImport::Exec(void)
{
    uint64_t objects = 0;

    libImport import(magic_db, mime_file);
    libImport_Result results;

    for (int i = optind; i < argc; i++)
        import.Scan(argv[i], results, recursive);

    for (libImport_Result::iterator ri = results.begin();
        ri != results.end(); ri++) {
        libOutput(O_DEBUG, "%s: %s\n",
            ri->first.c_str(), ri->second->mime_type.c_str());
        for (vector<string>::iterator si = ri->second->path.begin();
            si != ri->second->path.end(); si++) {
            libOutput(O_DEBUG, "  %s\n", (*si).c_str());
            objects++;
        }
    }

    libOutput(O_INFO, "Found %d objects, %d unique.\n",
        objects, results.size());

    import.FreeResult(results);

    return 0;
}

int main(int argc, char *argv[])
{
    mdbImport mdb_import(argc, argv);
    return (mdb_import.Exec());
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
