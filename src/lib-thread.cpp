#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <string>
#include <stdexcept>

#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <syslog.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

using namespace std;

#include <media-db/exception.h>
#include <media-db/util.h>
#include <media-db/thread.h>

static void *libThread_entry(void *param)
{
    int rc;

    sigset_t signal_set;
    sigfillset(&signal_set);
    sigdelset(&signal_set, SIGPROF);

    if ((rc = pthread_sigmask(SIG_BLOCK, &signal_set, NULL)) != 0)
        throw libSystemException( __PRETTY_FUNCTION__, "pthread_sigmask", rc);

    libThread *thread = reinterpret_cast<libThread *>(param);
    return thread->Entry();
}

libThread::libThread(int stack_size)
    : id(0)
{
    int rc;

    if ((rc = pthread_attr_init(&attr)) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_attr_init", rc);
    if ((rc = pthread_attr_setstacksize(&attr, stack_size)) != 0) {
        throw libSystemException(
            __PRETTY_FUNCTION__, "pthread_attr_setstacksize", rc);
    }
}

libThread::~libThread()
{
    id = 0;
    pthread_attr_destroy(&attr);
}

void libThread::SetName(const string &name)
{
    this->name = name;

    if (IsValid()) {
        char _name[16];
        strncpy(_name, name.c_str(), sizeof(_name) - 1);
        pthread_setname_np(id, _name);
    }
}

void libThread::SetAffinity(int cpu)
{
    if (IsValid()) return;

    long cpus = ::libUtil_getcpus();

    if (cpu >= cpus) {
        libOutput(O_WARN, "%s: CPU %d >= %ld\n", __PRETTY_FUNCTION__, cpu, cpus);
        return;
    }

    cpu_set_t *cpuset = CPU_ALLOC(cpus);
    if (cpuset == NULL) {
        libOutput(O_WARN, "%s: Error allocating %ld CPU set.\n",
            __PRETTY_FUNCTION__, cpus);
        return;
    }

    size_t size = CPU_ALLOC_SIZE(cpus);

    CPU_ZERO_S(size, cpuset);
    CPU_SET_S(cpu, size, cpuset);

    int rc = pthread_attr_setaffinity_np(&attr,
        CPU_COUNT_S(size, cpuset), cpuset);

    CPU_FREE(cpuset);

    if (rc != 0) {
        throw libSystemException( __PRETTY_FUNCTION__,
            "pthread_attr_setaffinity_np", rc);
    }
}

void libThread::Create(void)
{
    if (IsValid()) return;

    int rc = pthread_create(
        &id, &attr, &libThread_entry, (void *)this);

    if (rc != 0) {
        id = 0;
        throw libSystemException( __PRETTY_FUNCTION__, "pthread_create", rc);
    }
}

void libThread::Join(void)
{
    if (!IsValid()) return;

    int rc = pthread_join(id, NULL);

    if (rc != 0) {
        id = 0;
        throw libSystemException( __PRETTY_FUNCTION__, "pthread_join", rc);
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
