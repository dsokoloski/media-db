#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cassert>
#include <stdexcept>

#include <sys/stat.h>

#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include <openssl/sha.h>

using namespace std;

#include <media-db/exception.h>
#include <media-db/magic.h>
#include <media-db/util.h>
#include <media-db/thread.h>
#include <media-db/import.h>

libImport_Worker::libImport_Worker(
    int cpu, pthread_mutex_t *queue_mutex, queue<string> *queue_file,
    const string &magic_db, const string &mime_file)
    : libThread(_libImport_THREAD_STACK_SIZE),
    cpu(cpu), control(libImport_Worker_NOP), path_buffer(NULL),
    queue_mutex(queue_mutex), queue_file(queue_file), progress(0.0f)
{
    int rc;

    if ((rc = pthread_mutex_init(&status_mutex, NULL)) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_mutex_init", rc);

    control = libImport_Worker_NOP;

    this->magic_db = magic_db.c_str();
    this->mime_file = mime_file.c_str();

    path_buffer = new char[PATH_MAX];
    memset(path_buffer, 0, PATH_MAX);
}

libImport_Worker::~libImport_Worker()
{
    if (path_buffer != NULL) delete [] path_buffer;
    pthread_mutex_destroy(&status_mutex);
}

void *libImport_Worker::Entry(void)
{
    libMagic magic(magic_db, mime_file, MAGIC_MIME_TYPE);

    int fd;
    long page_size = ::libUtil_getpagesize();
    SHA_CTX sha1;
    uint8_t sha1_result[SHA_DIGEST_LENGTH];
    uint8_t sha1_buffer[page_size];
    struct stat st_info;

    char worker_id[16];
    snprintf(worker_id, sizeof(worker_id), "import #%02d", cpu);
    pthread_setname_np(pthread_self(), worker_id);

    string path = "";

    libOutput(O_DEBUG, "%s init\n", worker_id);

    while (__sync_fetch_and_add(
        (int *)&control, 0) != libImport_Worker_RUN) {
        usleep(250000);
    }

    while (__sync_fetch_and_add(
        (int *)&control, 0) == libImport_Worker_RUN) {

        path.clear();

        pthread_mutex_lock(queue_mutex);

        if (!queue_file->empty()) {
            path = queue_file->front();
            queue_file->pop();
        }

        pthread_mutex_unlock(queue_mutex);

        if (path.empty()) break;

        pthread_mutex_lock(&status_mutex);
        if (realpath(path.c_str(), path_buffer) == NULL) {
            libOutput(O_ERROR, "%s: realpath: %s: %s\n",
                worker_id, path.c_str(), strerror(errno));
            pthread_mutex_unlock(&status_mutex);
            continue;
        }

        const char *mime_type = magic.IdentifyByMagic(path_buffer);
        if (mime_type == NULL ||
            !strcmp(mime_type, "application/octet-stream"))
            mime_type = magic.IdentifyByExtension(path_buffer);

        if ((fd = open(path_buffer, O_RDONLY)) < 0) {
            libOutput(O_ERROR, "%s: open: %s: %s\n",
                worker_id, path_buffer, strerror(errno));
            pthread_mutex_unlock(&status_mutex);
            continue;
        }

        lstat(path_buffer, &st_info);

        pthread_mutex_unlock(&status_mutex);

        SHA1_Init(&sha1);

        off_t bytes_read = 0;

        for ( ;; ) {
            ssize_t bytes;
            if ((bytes = read(fd, sha1_buffer, page_size)) <= 0) break;
            SHA1_Update(&sha1, sha1_buffer, bytes);
            bytes_read += bytes;

            pthread_mutex_lock(&status_mutex);
            progress = (float)bytes_read * 100.0f / (float)st_info.st_size;
            pthread_mutex_unlock(&status_mutex);
        }

        close(fd);
        SHA1_Final(sha1_result, &sha1);

        string hash;
        libUtil_bin2hex(sha1_result, hash, SHA_DIGEST_LENGTH);

        libOutput(O_DEBUG, "%s: hash: %s, type/file: %s %s\n",
            worker_id, hash.c_str(), mime_type, path_buffer);

        libImport_ResultFile *entry;
        libImport_Result::iterator i = result.find(hash);
        if (i == result.end()) {
            entry = new libImport_ResultFile;
            entry->mime_type = mime_type;
            result[hash] = entry;
        }
        else entry = i->second;

        entry->path.push_back(path_buffer);
    }

    libOutput(O_DEBUG, "%s: exit\n", worker_id);

    return (void *)NULL;
}

void libImport_Worker::GetStatus(int &cpu, string &status, float &progress)
{
    pthread_mutex_lock(&status_mutex);
    cpu = this->cpu;
    progress = this->progress;
    status = this->path_buffer;
    pthread_mutex_unlock(&status_mutex);
}

static void libImport_scan(libImport_ScanState *state)
{
    DIR *dh;
    
    if ((dh = opendir(state->buffer.c_str())) == NULL) {
        libOutput(O_ERROR,
            "opendir: %s: %s\n", state->buffer.c_str(), strerror(errno));
        return;
    }

    string full_path(state->buffer);

    struct dirent *entry;
    while ((entry = readdir(dh)) != NULL) {
        state->buffer.assign(full_path);
        state->buffer.append("/");
        state->buffer.append(entry->d_name);

        if (lstat(state->buffer.c_str(), &state->st_info) != 0) {
            libOutput(O_ERROR,
                "lstat: %s: %s\n", state->buffer.c_str(), strerror(errno));
            continue;
        }

        if (S_ISDIR(state->st_info.st_mode) &&
            state->recursive_scan) {
            if (ISDOT(entry->d_name)) continue;
            libImport_scan(state);
        }
        else if (S_ISREG(state->st_info.st_mode))
            state->queue_file->push(state->buffer);
    }

    closedir(dh);
}

libImport::libImport(const string &magic_db, const string &mime_file)
    : magic_db(magic_db), mime_file(mime_file), queue_mutex(NULL)
{
    Reset();
}

libImport::~libImport()
{
    Reset();

    if (queue_mutex) {
        pthread_mutex_destroy(queue_mutex);
        delete queue_mutex;
    }
}

void libImport::Reset(void)
{
    path = NULL;
}

int libImport::Scan(const char *path, libImport_Result &result, bool recursive)
{
    int rc, cpu;
    float progress;
    string status;
    long cpus = ::libUtil_getcpus();

    if (!queue_mutex) {
        queue_mutex = new pthread_mutex_t;
        if (!queue_mutex) return -1;
        if ((rc = pthread_mutex_init(queue_mutex, NULL)) != 0) {
            delete queue_mutex;
            queue_mutex = NULL;
            return rc;
        }
    }

    for (int cpu = 0; cpu < (int)cpus; cpu++) {

        libImport_Worker *worker;

        try {
            worker = new libImport_Worker(
                cpu, queue_mutex, &queue_file, magic_db, mime_file);
            worker->SetAffinity(cpu);
            worker->Create();
        } catch (runtime_error &e) {
            libOutput(O_ERROR, "Exception creating import worker: %s\n", e.what());
            for (vector<libImport_Worker *>::iterator i = workers.begin();
                i != workers.end(); i++) delete (*i);
            return 1;
        }

        workers.push_back(worker);
    }

    libOutput(O_DEBUG, "spawned %ld worker threads\n", cpus);
    usleep(2000);

    libImport_ScanState scan_state;
    scan_state.buffer.assign(path);
    scan_state.queue_file = &queue_file;
    scan_state.recursive_scan = recursive;

    libOutput(O_DEBUG, "object scan: %s\n", path);
    libImport_scan(&scan_state);
    libOutput(O_DEBUG, "object scan: %s (%d objects found)\n",
        path, queue_file.size());

    size_t queue_size_last = queue_file.size();

    for (vector<libImport_Worker *>::iterator i = workers.begin();
        i != workers.end(); i++) (*i)->Start();

    for ( ;; ) {
        size_t queue_size = 0;

        pthread_mutex_lock(queue_mutex);
        queue_size = queue_file.size();
        pthread_mutex_unlock(queue_mutex);

        libOutput(O_DEBUG, "object queue size: %d (%d objects/s)\n",
            queue_size, (queue_size_last - queue_size));

        if (queue_size_last) queue_size_last = queue_size;
        if (queue_size == 0) break;

        for (vector<libImport_Worker *>::iterator i = workers.begin();
            i != workers.end(); i++) {

            (*i)->GetStatus(cpu, status, progress);

            if (status.length()) {
                libOutput(O_INFO, "Worker: %02d: progress: %6.02f%% %s\n",
                    cpu, progress, status.c_str());
            }
        }

        sleep(1);
    }

    for (vector<libImport_Worker *>::iterator i = workers.begin();
        i != workers.end(); i++) (*i)->Stop();

    for (vector<libImport_Worker *>::iterator i = workers.begin();
        i != workers.end(); i++) {

        (*i)->Join();

        libImport_Result::iterator ri = (*i)->result.begin();
        for ( ; ri != (*i)->result.end(); ri++) {
            libImport_Result::iterator fi = result.find(ri->first);
            if (fi == result.end()) {
                result[ri->first] = ri->second;
                continue;
            }

            result[ri->first]->path.insert(
                result[ri->first]->path.begin(),
                ri->second->path.begin(),
                ri->second->path.end());

            delete ri->second;
        }
    }

    for (vector<libImport_Worker *>::iterator i = workers.begin();
        i != workers.end(); i++) delete (*i);
    workers.clear();

    return 0;
}

void libImport::FreeResult(libImport_Result &result)
{
    for (libImport_Result::iterator ri = result.begin(); ri != result.end(); ri++)
        delete ri->second;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
