#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>
#include <deque>

#include <unistd.h>
#include <syslog.h>
#include <stdint.h>
#include <getopt.h>
#include <netdb.h>

#include <sys/stat.h>
#include <sys/socket.h>

#include <event2/event.h>

using namespace std;

#include <media-db/util.h>
#include <media-db/thread.h>
#include <media-db/event.h>
#include <media-db/main.h>
#include <media-db/socket.h>

#include "mdb-server.h"

mdbServer::mdbServer(int argc, char * const *argv)
    : libMain(argc, argv)
{
/*
    libMainOption *option;

    // Help: -h, --help
    option = new libMainOption;
    option->long_opt = "help";
    option->short_opt = 'h';
    option->has_arg = 0;
    option->help = "Display usage help text.";

    options.insert(options.begin(), option);
*/
    ParseArguments();

    libOutputContext ctx;

    ctx.type = libOutput_SYSLOG;
    ctx.debug = debug;
    ctx.syslog_facility = LOG_LOCAL0;
    ctx.syslog_options = LOG_PERROR | LOG_PID;
    ctx.syslog_ident = prog_name;

    libOutputFree();
    libOutputInit(ctx);
}

mdbServer::~mdbServer()
{
}

int mdbServer::Exec(void)
{
    try {
        libSocketServerLocal server("/tmp/test-socket");
        //libSocketServerRemote server("127.0.0.1", "5500");
        //libSocketServerRemote server("::1", "5500");
        //libSocketServerRemote server("", "5500");
        libSocket *peer = server.Accept();
        delete peer;
    } catch (exception &e) {
        libOutput(O_ERROR, "Exception: %s\n", e.what());
//        exit(1);
    }

    gEventServer.Dispatch();

    for (int i = 0; i < 10; i++) {
        libOutput(O_DEBUG, "tick\n");
        sleep(1);
    }

    return 0;
}

int main(int argc, char *argv[])
{
    mdbServer mdb_server(argc, argv);
    mdb_server.Daemonize();

    libEvent_Server::Start();
    gEventServer.Register(&mdb_server);

    libMain_SignalHandler::Start();

    int rc = mdb_server.Exec();

    gEventServer.Unregister(&mdb_server);

    return rc;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
