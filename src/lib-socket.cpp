#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <sstream>
#include <stdexcept>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <linux/un.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>

using namespace std;

#include <media-db/exception.h>
#include <media-db/socket.h>
#include <media-db/util.h>

libGetAddrInfoException::libGetAddrInfoException(
    const string &where_arg, const string &what_arg, int why_arg) throw()
    : runtime_error(what_arg),
    where_arg(where_arg), what_arg(what_arg), why_arg(why_arg), message(NULL)
{
    ostringstream os;
    os <<
        where_arg << ": " <<
        what_arg  << ": " <<
        gai_strerror(why_arg);
    message = strdup(os.str().c_str());
}

libGetAddrInfoException::~libGetAddrInfoException() throw()
{
    if (message != NULL) free((void *)message);
}

const char *libGetAddrInfoException::what() const throw()
{
    return message;
}

libSocketLocal::libSocketLocal(libSocket *base, const string &node)
    : base(base)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    struct sockaddr_un *sa_un = new struct sockaddr_un;

    base->node = node;
    base->sa_size = sizeof(struct sockaddr_un);
    base->sa = (sockaddr *)sa_un;

    memset(sa_un, 0, base->sa_size);

    sa_un->sun_family = base->family = AF_LOCAL;
    strncpy(sa_un->sun_path, base->node.c_str(), UNIX_PATH_MAX);

    int rc;

    if ((rc = IsValid()) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, node, rc);

    base->Create();
}

libSocketLocal::~libSocketLocal()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

int libSocketLocal::IsValid(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    struct stat socket_stat;

    if (base->type == libSocket::LST_CLIENT) {
        stat(base->node.c_str(), &socket_stat);
        return errno;
    }
    else if (base->type == libSocket::LST_SERVER) {
        int rc = 0;
        long max_path_len = 4096;

        max_path_len = pathconf(base->node.c_str(), _PC_PATH_MAX);
        if (max_path_len == -1) return errno;

        FILE *fh = fopen(_LIB_SOCKET_PATH_UNIX, "r");
        if (!fh) return errno;

        for ( ;; ) {
            char filename[max_path_len];
            int a, b, c, d, e, f, g;
            int count = fscanf(fh, "%x: %u %u %u %u %u %u ",
                &a, &b, &c, &d, &e, &f, &g);
            if (count == 0) {
                if (!fgets(filename, max_path_len, fh)) break;
                continue;
            }
            else if (count == -1) break;
            else if (!fgets(filename, max_path_len, fh)) break;
            else if (strncmp(filename, base->node.c_str(), base->node.size()) == 0) {
                rc = EADDRINUSE;
                break;
            }
        }

        fclose(fh);

        if (rc != 0) return rc;

        if (stat(base->node.c_str(), &socket_stat) != 0 && errno != ENOENT)
            return errno;

        if (errno != ENOENT && unlink(base->node.c_str()) != 0)
            return errno;
    }

    return 0;
}

libSocketRemote::libSocketRemote(libSocket *base, const string &node, const string &service)
    : base(base)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    base->node = node;
    base->service = service;

    base->Create();
}

libSocketRemote::~libSocketRemote()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketClient::libSocketClient(libSocket *base)
    : base(base)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    base->type = libSocket::LST_CLIENT;
}

libSocketClient::~libSocketClient()
{
}

libSocket *libSocketServer::Accept(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    libSocket *peer = NULL;
    int peer_sd = -1;
    socklen_t peer_sa_size = 0;
	sockaddr *peer_sa = NULL;

    if (base->sa_size == sizeof(struct sockaddr_un)) {
        peer_sa = (sockaddr *)new struct sockaddr_un;
        peer_sa_size = sizeof(struct sockaddr_un);
    }
    else {
        peer_sa = (sockaddr *)new struct sockaddr_storage;
        peer_sa_size = sizeof(struct sockaddr_storage);
    }

    try {
        peer_sd = accept(base->sd, peer_sa, &peer_sa_size);
        if (peer_sd < 0)
            throw libSystemException(__PRETTY_FUNCTION__, "accept", errno);

        if (base->sa_size == sizeof(struct sockaddr_un)) {
            peer = new libSocket(base->node);

            libOutput(O_DEBUG, "%s: peer: %s\n", __PRETTY_FUNCTION__, base->node.c_str());
        }
        else {
            char node[NI_MAXHOST], service[NI_MAXSERV];

            int rc = getnameinfo(peer_sa, peer_sa_size, node, NI_MAXHOST,
                service, NI_MAXSERV, NI_NUMERICHOST | NI_NUMERICSERV);

            if (rc != 0) 
                throw libGetAddrInfoException(__PRETTY_FUNCTION__, "getnameinfo", rc);

            peer = new libSocket(node, service);
    
            libOutput(O_DEBUG, "%s: peer: %s:%s\n", __PRETTY_FUNCTION__, node, service);
        }

        peer->sd = peer_sd;
        peer->family = base->family;
        peer->type = libSocket::LST_CLIENT;
        peer->state = libSocket::LSS_ACCEPTED;
    }
    catch (runtime_error &e) {
        if (peer != NULL) {
            delete peer;
            peer = NULL;
        }
        else if (peer_sa != NULL)
            delete peer_sa;
        if (peer_sd >= 0) close(peer_sd);
        throw;
    }

    return peer;
}

libSocketServer::libSocketServer(libSocket *base)
    : base(base)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    base->type = libSocket::LST_SERVER;
}

libSocketServer::~libSocketServer()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketClientLocal::libSocketClientLocal(const string &node)
    : libSocketClient(this), libSocketLocal(this, node)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

}

libSocketClientLocal::~libSocketClientLocal()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketServerLocal::libSocketServerLocal(const string &node)
    : libSocketServer(this), libSocketLocal(this, node)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketServerLocal::~libSocketServerLocal()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketClientRemote::libSocketClientRemote(const string &node, const string &service)
    : libSocketClient(this), libSocketRemote(this, node, service)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketClientRemote::~libSocketClientRemote()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketServerRemote::libSocketServerRemote(const string &node, const string &service)
    : libSocketServer(this), libSocketRemote(this, node, service)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocketServerRemote::~libSocketServerRemote()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocket::libSocket()
    : sd(1), family(AF_UNSPEC), sa(NULL), sa_size(0),
    type(LST_NULL), state(LSS_INIT), bytes_in(0), bytes_out(0)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocket::libSocket(const string &node)
    : sd(-1), family(AF_UNSPEC), sa(NULL), sa_size(0),
    node(node), type(LST_NULL), state(LSS_INIT), bytes_in(0), bytes_out(0)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocket::libSocket(const string &host, const string &service)
    : sd(-1), family(AF_UNSPEC), sa(NULL), sa_size(0),
    node(host), service(service), type(LST_NULL), state(LSS_INIT),
    bytes_in(0), bytes_out(0)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

libSocket::~libSocket()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    if (sd != -1) close(sd);
    if (sa != NULL) delete sa;

    if (type == LST_SERVER && family == AF_LOCAL)
        unlink(node.c_str());
}

ssize_t libSocket::Read(uint8_t *buffer, ssize_t length)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    return 0;
}

ssize_t libSocket::Write(uint8_t *buffer, ssize_t length)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    return 0;
}

ssize_t libSocket::BlockingRead(uint8_t *buffer, ssize_t length)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    return 0;
}

ssize_t libSocket::BlockingWrite(uint8_t *buffer, ssize_t length)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    return 0;
}

void libSocket::Create(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    if (family == AF_UNSPEC) {
        struct addrinfo hints;
        struct addrinfo *result, *rp;

        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_INET6;
        //hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_V4MAPPED;
        //hints.ai_flags = AI_V4MAPPED | AI_ALL;
        if (type == LST_SERVER)
            hints.ai_flags |= AI_PASSIVE;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_canonname = NULL;
        hints.ai_addr = NULL;
        hints.ai_next = NULL;
        
        int rc;
        const char *_node = (node.length()) ? node.c_str() : NULL;
        if ((rc = getaddrinfo(_node, service.c_str(), &hints, &result)) != 0)
            throw libGetAddrInfoException(__PRETTY_FUNCTION__, "getaddrinfo", rc);

        sd = -1;
        for (rp = result; rp != NULL; rp = rp->ai_next) {
            sd = socket(rp->ai_family,
                rp->ai_socktype | SOCK_NONBLOCK, rp->ai_protocol);
            if (sd < 0) {
                libOutput(O_DEBUG, "%s: socket: %s",
                    __PRETTY_FUNCTION__, strerror(errno));
                continue;
            }

            if (type == LST_CLIENT) {
                if (connect(sd, rp->ai_addr, rp->ai_addrlen) == 0) {
                    libOutput(O_DEBUG, "%s: connected\n", __PRETTY_FUNCTION__);
                    break;
                }
                else {
                    if (rp->ai_family == AF_INET) {
                        libOutput(O_DEBUG, "%s: connect v4: %s\n",
                            __PRETTY_FUNCTION__, strerror(errno));
                    }
                    else if (rp->ai_family == AF_INET6) {
                        libOutput(O_DEBUG, "%s: connect v6: %s\n",
                            __PRETTY_FUNCTION__, strerror(errno));
                    }
                    else {
                        libOutput(O_DEBUG, "%s: connect: %s\n",
                            __PRETTY_FUNCTION__, strerror(errno));
                    }
                }
            }
            else if (type == LST_SERVER) {
                int on = 1;
                if (setsockopt(sd,
                    SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on)) != 0) {
                    throw libSystemException(__PRETTY_FUNCTION__,
                        "setsockopt: SO_REUSEADDR", errno);
                }

                if (bind(sd, rp->ai_addr, rp->ai_addrlen) == 0) break;
                else {
                    libOutput(O_DEBUG, "%s: bind: %s",
                        __PRETTY_FUNCTION__, strerror(errno));
                }
            }

            close(sd); sd = -1;
        }

        if (rp == NULL) {
            freeaddrinfo(result);
            throw libException(__PRETTY_FUNCTION__, "no addresses found");
        }

        family = rp->ai_family;
        sa_size = rp->ai_addrlen;
        sa = (sockaddr *)new uint8_t[sa_size];
        memcpy(sa, rp->ai_addr, sa_size);

        freeaddrinfo(result);

        if (sd < 0) {
            throw libException(__PRETTY_FUNCTION__, "unable to create socket");
        }

        if (type == LST_SERVER) {
            if (listen(sd, SOMAXCONN) != 0)
                throw libSystemException(__PRETTY_FUNCTION__, "listen", errno);
        }
    }
    else if (family == AF_LOCAL) {
        if ((sd = socket(family, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0)
            throw libSystemException(__PRETTY_FUNCTION__, "socket", errno);

        if (type == LST_CLIENT) {
            if (connect(sd, sa, sa_size) != 0)
                throw libSystemException(__PRETTY_FUNCTION__, "connect", errno);
            libOutput(O_DEBUG, "%s: connected\n", __PRETTY_FUNCTION__);
        }
        else if (type == LST_SERVER) {
            if (bind(sd, sa, sa_size) != 0)
                throw libSystemException(__PRETTY_FUNCTION__, "bind", errno);

            if (listen(sd, SOMAXCONN) != 0)
                throw libSystemException(__PRETTY_FUNCTION__, "listen", errno);
        }
    }

    libOutput(O_DEBUG, "%s: created\n", __PRETTY_FUNCTION__);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
