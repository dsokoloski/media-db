#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <sstream>
#include <stdexcept>

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>

using namespace std;

#include <media-db/exception.h>

libException::libException(const string &where_arg, const string &what_arg) throw()
    : runtime_error(what_arg), where_arg(where_arg), what_arg(what_arg), message(NULL)
{
    ostringstream os;
    os << where_arg << ": " << what_arg;
    message = strdup(os.str().c_str());
}

libException::~libException() throw()
{
    if (message != NULL) free((void *)message);
}

const char *libException::what() const throw()
{
    return message;
}

libSystemException::libSystemException(
    const string &where_arg, const string &what_arg, int why_arg) throw()
    : runtime_error(what_arg),
    where_arg(where_arg), what_arg(what_arg), why_arg(why_arg), message(NULL)
{
    ostringstream os;
    os << where_arg << ": " << what_arg << ": " << strerror(why_arg);
    message = strdup(os.str().c_str());
}

libSystemException::~libSystemException() throw()
{
    if (message != NULL) free((void *)message);
}

const char *libSystemException::what() const throw()
{
    return message;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
