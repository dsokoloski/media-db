#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <deque>
#include <cassert>
#include <stdexcept>

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>
#include <stdbool.h>
#include <libgen.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

using namespace std;

#include <media-db/exception.h>
#include <media-db/util.h>
#include <media-db/thread.h>
#include <media-db/event.h>
#include <media-db/main.h>

static void libMain_atexit(void)
{
#ifndef NDEBUG
    cerr << __PRETTY_FUNCTION__ << endl;
#endif
    libOutputFree();
}

void *libMain_SignalHandler::Entry(void)
{
    int sig;
    siginfo_t si;
    sigset_t signal_set;

    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGHUP);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGPIPE);
    sigaddset(&signal_set, SIGCHLD);
    sigaddset(&signal_set, SIGALRM);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGUSR2);

    SetName("signal handler");

    for ( ;; ) {
        sig = sigwaitinfo(&signal_set, &si);
        if (sig < 0) {
            libOutput(O_ERROR, "sigwaitinfo: %s\n", strerror(errno));
            if (errno == EINTR) {
                usleep(100 * 1000);
                continue;
            }
            return NULL;
        }

        libOutput(O_DEBUG, "Caught signal: %s\n", strsignal(sig));

        switch (sig) {
        case SIGINT:
        case SIGTERM:
            exit(0);
        }
    }

    return (void *)NULL;
}

void libMain_SignalHandler::Start(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    int rc;

    sigset_t signal_set;
    sigfillset(&signal_set);
    sigdelset(&signal_set, SIGPROF);

    if ((rc = pthread_sigmask(SIG_BLOCK, &signal_set, NULL)) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_sigmask", rc);

    libMain_SignalHandler *thread = new libMain_SignalHandler;
    thread->Create();
}

libMain::libMain(int argc, char * const *argv)
    : argc(argc), argv(argv), verbose(false), debug(false)
{
    atexit(libMain_atexit);

    libMainOption *option;

    option = new libMainOption;
    option->long_opt = "verbose";
    option->short_opt = 'v';
    option->has_arg = 0;
    option->help = "Verbose output.";
    options.push_back(option);

    option = new libMainOption;
    option->long_opt = "debug";
    option->short_opt = 'd';
    option->has_arg = 0;
    option->help = "Enable debug mode (also enables --verbose output).";
    options.push_back(option);

    option = new libMainOption;
    option->long_opt = "help";
    option->short_opt = 'h';
    option->has_arg = 0;
    option->help = "Display this usage help text.";
    options.push_back(option);

    prog_name = basename(argv[0]);
    prog_usage = "[<options>]";

    libOutputContext ctx;
    ctx.type = libOutput_STDERR;
    ctx.debug = true;

    libOutputInit(ctx);
}

libMain::~libMain()
{
    for (vector<libMainOption *>::iterator i = options.begin();
        i != options.end(); i++) delete (*i);
}

void libMain::Daemonize(void)
{
    if (debug) return;
    if (daemon(0, 0) < 0)
        libOutput(O_ERROR, "daemon: %s\n", strerror(errno));
}

void libMain::Usage(void)
{
    libOutput(O_INFO, "%s v%s Copyright (C) 2013-%s Darryl Sokoloski [%s]\n",
        PACKAGE_NAME, PACKAGE_VERSION, COPYRIGHT_YEAR, PACKAGE_URL);

    libOutput(O_INFO, prog_name.c_str());
    if (prog_usage.size())
        libOutput(O_INFO, " %s", prog_usage.c_str());
    libOutput(O_INFO, "\n");

    for (vector<libMainOption *>::iterator i = options.begin();
        i != options.end(); i++) {
        libOutput(O_INFO, "  -%c, --%s", (*i)->short_opt, (*i)->long_opt.c_str());
        if ((*i)->arg_name.size() && (*i)->has_arg) {
            if ((*i)->has_arg == 2)
                libOutput(O_INFO, " [<%s>]", (*i)->arg_name.c_str());
            else if ((*i)->has_arg == 1)
                libOutput(O_INFO, " <%s>", (*i)->arg_name.c_str());
        }
        libOutput(O_INFO, "\n        %s\n", (*i)->help.c_str());
        if ((*i)->default_value.size())
            libOutput(O_INFO, "        Default: %s\n", (*i)->default_value.c_str());
    }
    if (prog_help.size())
        libOutput(O_INFO, "\n%s\n", prog_help.c_str());
}

void libMain::ParseArguments(void)
{
    int rc, o = 0;
    struct option getopt_options[_MDB_MAX_OPTIONS + 1];
    ostringstream flags;

    for (vector<libMainOption *>::iterator i = options.begin();
        i != options.end() && o < _MDB_MAX_OPTIONS; i++, o++) {

        getopt_options[o].name = (*i)->long_opt.c_str();
        getopt_options[o].has_arg = (*i)->has_arg;
        getopt_options[o].flag = NULL;
        getopt_options[o].val = (int)(*i)->short_opt;

        switch((*i)->has_arg) {
        case 0:
            flags << (*i)->short_opt;
            break;
        case 1:
            flags << (*i)->short_opt << ":";
            break;
        case 2:
            flags << (*i)->short_opt << "::";
            break;
        }
    }
    getopt_options[o].name = NULL;

    for (optind = 1;; ) {
        if ((rc = getopt_long(argc, argv,
            flags.str().c_str(), getopt_options, &o)) == -1) break;
        switch (rc) {
        case '?':
            libOutput(O_ERROR, "Try --help for usage information.\n");
            exit(1);
        case 'h':
            Usage();
            exit(1);
        default:
            SetOption(rc, optarg);
        }
    }
}

void libMain::SetOption(char option, const char *optarg)
{
    libMainOption *o = NULL;
    for (vector<libMainOption *>::iterator i = options.begin();
        i != options.end(); i++) {
        if ((*i)->short_opt == option) {
            o = (*i);
            break;
        }
    }

    if (o == NULL) return;

    switch (option) {
    case 'd':
        debug = true;
        verbose = true;
        break;
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
