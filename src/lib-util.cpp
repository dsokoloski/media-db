#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <errno.h>
#include <pthread.h>

#include <string>
#include <iostream>

using namespace std;

#include <media-db/util.h>

static libOutputContext *libOutputCtx = NULL;

void libOutputInit(libOutputContext &ctx)
{
#ifndef NDEBUG
    cerr << __PRETTY_FUNCTION__ << endl;
#endif
    if (libOutputCtx != NULL) {
        cerr << __PRETTY_FUNCTION__ << ": already initialized!" << endl;
        return;
    }

    libOutputCtx = new libOutputContext(ctx);

    int rc;
    libOutputCtx->mutex = new pthread_mutex_t;
    if ((rc = pthread_mutex_init(libOutputCtx->mutex, NULL)) != 0) {
        cerr << __PRETTY_FUNCTION__ << ": pthread_mutex_init: " << strerror(errno) << endl;
        delete libOutputCtx;
        libOutputCtx = NULL;
        return;
    }

    if (libOutputCtx->type == libOutput_SYSLOG) {
        openlog(
            libOutputCtx->syslog_ident.c_str(),
            libOutputCtx->syslog_options,
            libOutputCtx->syslog_facility
        );
    }
}

void libOutputFree(void)
{
#ifndef NDEBUG
    cerr << __PRETTY_FUNCTION__ << endl;
#endif
    if (libOutputCtx->type == libOutput_SYSLOG)
        closelog();

    if (libOutputCtx != NULL) {
        pthread_mutex_destroy(libOutputCtx->mutex);
        delete libOutputCtx->mutex;
        delete libOutputCtx;
        libOutputCtx = NULL;
    }
}

void libOutput(libOutputLevel level, const char *format, ...)
{
    va_list ap;

    pthread_mutex_lock(libOutputCtx->mutex);

    if (level == libOutput_DEBUG && !libOutputCtx->debug) {
        pthread_mutex_unlock(libOutputCtx->mutex);
        return;
    }

    va_start(ap, format);

    if (libOutputCtx->type == libOutput_STDERR)
        vfprintf(stderr, format, ap);
    else if (libOutputCtx->type == libOutput_SYSLOG) {
        int priority;

        switch (level) {
        case libOutput_INFO:
            priority = libOutputCtx->syslog_facility | LOG_INFO;
            break;
        case libOutput_WARN:
            priority = libOutputCtx->syslog_facility | LOG_WARNING;
            break;
        case libOutput_ERROR:
            priority = libOutputCtx->syslog_facility | LOG_ERR;
            break;
        case libOutput_DEBUG:
            priority = libOutputCtx->syslog_facility | LOG_DEBUG;
            break;
        default:
            priority = libOutputCtx->syslog_facility | LOG_WARNING;
            break;
        }

        vsyslog(priority, format, ap);
    }

    va_end(ap);

    pthread_mutex_unlock(libOutputCtx->mutex);
}

void libUtil_bin2hex(const uint8_t *bin, string &hex, size_t length)
{
    char hexstr[3];

    hex.clear();
    for (size_t i = 0; i < length; i++) {
        sprintf(hexstr, "%02x", bin[i]);
        hex.append(hexstr);
    }
}

#ifndef _libUtil_NCPUS
#define _libUtil_NCPUS      2
#endif

long libUtil_getcpus(void)
{
    long cpus = sysconf(_SC_NPROCESSORS_CONF);

    if (cpus < 0)
        libOutput(O_WARN, "%s: %s\n", __PRETTY_FUNCTION__, strerror(errno));

    if (cpus <= 0)
        cpus = _libUtil_NCPUS;

    return cpus;
}

#ifndef _libUtil_PAGE_SIZE
#define _libUtil_PAGE_SIZE  4096
#endif

long libUtil_getpagesize(void)
{
    long page_size = sysconf(_SC_PAGESIZE);
    if (page_size < 0)
        libOutput(O_WARN, "%s: %s\n", __PRETTY_FUNCTION__, strerror(errno));

    if (page_size <= 0)
        page_size = _libUtil_PAGE_SIZE;

    return page_size;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
