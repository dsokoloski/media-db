#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdexcept>
#include <cassert>
#include <map>
#include <deque>

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

using namespace std;

#include <media-db/exception.h>
#include <media-db/thread.h>
#include <media-db/util.h>
#include <media-db/event.h>

static void libEvent_log_cb(int severity, const char *message)
{
    switch (severity) {
    case EVENT_LOG_DEBUG:
        libOutput(O_DEBUG, "libevent: %s\n", message);
        break;
    case EVENT_LOG_MSG:
        libOutput(O_INFO, "libevent: %s\n", message);
        break;
    case EVENT_LOG_WARN:
        libOutput(O_WARN, "libevent: %s\n", message);
        break;
    case EVENT_LOG_ERR:
    default:
        libOutput(O_ERROR, "libevent: %s\n", message);
        break;
    }
}

libEvent::libEvent()
{
}

libEvent::~libEvent()
{
}

libEvent_Client::libEvent_Client()
{
    int rc;

    if ((rc = pthread_mutex_init(&event_queue_mutex, NULL)) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_mutex_init", rc);

    if ((rc = pthread_mutex_init(&event_condition_mutex, NULL)) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_mutex_init", rc);

    pthread_condattr_t cond_attr;

    if ((rc = pthread_condattr_init(&cond_attr)) != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_condattr_init", rc);

    pthread_condattr_setclock(&cond_attr, CLOCK_MONOTONIC);

    rc = pthread_cond_init(&event_condition, &cond_attr);
    pthread_condattr_destroy(&cond_attr);

    if (rc != 0)
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_cond_init", rc);
}

libEvent_Client::~libEvent_Client()
{
    pthread_mutex_destroy(&event_queue_mutex);
    pthread_cond_destroy(&event_condition);
    pthread_mutex_destroy(&event_condition_mutex);
}

void libEvent_Client::EventPush(libEvent *event)
{
    pthread_mutex_lock(&event_queue_mutex);
    event_queue.push_back(event);
    pthread_mutex_unlock(&event_queue_mutex);
}

libEvent_Server::libEvent_Server()
    : libThread(_libEvent_STACK_SIZE), eb(NULL)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    event_set_log_callback(libEvent_log_cb);
#ifndef NDEBUG
    event_enable_debug_mode();
#endif
    libOutput(O_DEBUG, "%s: libevent version: %s\n",
        __PRETTY_FUNCTION__, event_get_version());

    struct event_config *ec;
    if ((ec = event_config_new()) == NULL)
        throw libSystemException( __PRETTY_FUNCTION__, "event_config_new", errno);

    event_config_set_num_cpus_hint(ec, ::libUtil_getcpus());

    eb = event_base_new_with_config(ec);
    event_config_free(ec);

    if (eb == NULL) {
        throw libSystemException( __PRETTY_FUNCTION__,
            "event_base_new_with_config", errno);
    }

    libOutput(O_DEBUG, "%s: event_base_get_method: %s\n",
        __PRETTY_FUNCTION__, event_base_get_method(eb));

    int rc;
    if ((rc = pthread_mutex_init(&client_map_mutex, NULL)) != 0) {
        event_base_free(eb);
        throw libSystemException(__PRETTY_FUNCTION__, "pthread_mutex_init", rc);
    }
}

libEvent_Server::~libEvent_Server()
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    Join();

    event_base_free(eb);
//    libevent_global_shutdown();
    pthread_mutex_destroy(&client_map_mutex);
}

void *libEvent_Server::Entry(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);

    SetName("event server");

    for ( ;; ) {
        //event_base_dump_events(eb, stdout);
        libOutput(O_DEBUG, "%s: tick\n", __PRETTY_FUNCTION__);
        sleep(1);
    }

    return (void *)NULL;
}

void libEvent_Server::Register(libEvent_Client *client)
{
    pthread_mutex_lock(&client_map_mutex);
    libEvent_ClientMap::iterator i = clients.find(client);
    if (i != clients.end()) {
        libOutput(O_WARN, "%s: client already registered: %p\n",
            __PRETTY_FUNCTION__, client);
    }
    else {
        clients[client] = client;
#ifndef NDEBUG
        libOutput(O_DEBUG, "%s: %p\n", __PRETTY_FUNCTION__, client);
#endif
    }
    pthread_mutex_unlock(&client_map_mutex);
}

void libEvent_Server::Unregister(libEvent_Client *client)
{
    pthread_mutex_lock(&client_map_mutex);
    libEvent_ClientMap::iterator i = clients.find(client);
    if (i == clients.end()) {
        libOutput(O_WARN, "%s: client not registered: %p\n",
            __PRETTY_FUNCTION__, client);
    }
    else {
        clients.erase(client);
#ifndef NDEBUG
        libOutput(O_DEBUG, "%s: %p\n", __PRETTY_FUNCTION__, client);
#endif
    }
    pthread_mutex_unlock(&client_map_mutex);
}

void libEvent_Server::Start(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
    libEvent_Server *thread = new libEvent_Server;
    thread->Create();
}

void libEvent_Server::Dispatch(void)
{
    libOutput(O_DEBUG, "%s\n", __PRETTY_FUNCTION__);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
