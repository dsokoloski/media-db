Media Database
==============

A personal media management project.

Cloning
-------

Ensure you enable the --recursive option when cloing, otherwise required submodules will be missing.

Example:
    
    # git clone --recursive git@github.com:dsokoloski/media-db.git

Preparing for Compilation
-------------------------

To update all submodule projects to their master branches, run the following from the top-level directory:

    # git submodule foreach git pull origin master

Example configure options:

    # ./configure --prefix=/usr --enable-static --disable-shared --disable-test --disable-doc --disable-wchar_t

