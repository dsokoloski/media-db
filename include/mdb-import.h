#ifndef __MDB_IMPORT_H
#define __MDB_IMPORT_H

class mdbImport : public libMain
{
public:
    mdbImport(int argc, char * const *argv);

    virtual ~mdbImport();

    virtual void SetOption(char option, const char *optarg);

    int Exec(void);

protected:
    string magic_db;
    string mime_file;
    bool recursive;
};

#endif // __MDB_IMPORT_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
