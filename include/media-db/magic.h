#ifndef _LIB_MAGIC_H
#define _LIB_MAGIC_H

#include <magic.h>

#ifndef MAGIC_VERSION
#error "Magic version not defined"
#endif

#if (MAGIC_VERSION != 529)
#error "Invalid magic version"
#endif

typedef map<string, string> libMagicMimeType;

class libMagic
{
public:
    libMagic(const char *magic_db, const char *mime_file, int flags);

    virtual ~libMagic();

    const char *IdentifyByMagic(const char *file);
    const char *IdentifyByExtension(const char *file);

protected:
    magic_t mdb;
    libMagicMimeType mime_types;
};

#endif // _LIB_MAGIC_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
