#ifndef _LIB_EXCEPTION_H
#define _LIB_EXCEPTION_H

using namespace std;

class libException : public runtime_error
{
public:
    explicit libException(
        const string &where_arg, const string &what_arg) throw();
    virtual ~libException() throw();

    virtual const char *what() const throw();

    string where_arg;
    string what_arg;
    const char *message;
};

class libSystemException : public runtime_error
{
public:
    explicit libSystemException(
        const string &where_arg, const string &what_arg, int why_arg) throw();
    virtual ~libSystemException() throw();

    virtual const char *what() const throw();

    string where_arg;
    string what_arg;
    int why_arg;
    const char *message;
};

#endif // __LIB_EXCEPTION_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
