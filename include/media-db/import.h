#ifndef _LIB_IMPORT_H
#define _LIB_IMPORT_H

#ifndef _libImport_THREAD_STACK_SIZE
#define _libImport_THREAD_STACK_SIZE    (32768 * 4)
#endif

typedef struct __libImport_ResultFile
{
    string mime_type;
    vector<string> path;
} libImport_ResultFile;

typedef map<string, libImport_ResultFile *> libImport_Result;

enum libImport_WorkerControl
{
    libImport_Worker_NOP,
    libImport_Worker_RUN,
};

class libImport_Worker : public libThread
{
public:
    libImport_Worker(
        int cpu, pthread_mutex_t *queue_mutex, queue<string> *queue_file,
        const string &magic_db, const string &mime_file);
    virtual ~libImport_Worker();

    virtual void *Entry(void);

    void Start(void) { __sync_fetch_and_add((int *)&control, libImport_Worker_RUN); }
    void Stop(void) { __sync_fetch_and_add((int *)&control, libImport_Worker_NOP); }

    void GetStatus(int &cpu, string &status, float &progress);

    libImport_Result result;

protected:
    int cpu;
    libImport_WorkerControl control;

    const char *magic_db;
    const char *mime_file;

    char *path_buffer;

    pthread_mutex_t *queue_mutex;
    queue<string> *queue_file;

    float progress;
    pthread_mutex_t status_mutex;
};

typedef struct __libImport_ScanState
{
    const char *path;
    struct stat st_info;
    string buffer;
    queue<string> *queue_file;
    bool recursive_scan;
} libImport_ScanState;

class libImport
{
public:
    libImport(const string &magic_db, const string &mime_file);

    virtual ~libImport();

    void Reset(void);
    int Scan(const char *path, libImport_Result &result, bool recursive = false);
    void Abort(void);

    void FreeResult(libImport_Result &result);

protected:
    void FreeWorker(libImport_Worker *worker);

    string magic_db;
    string mime_file;
    const char *path;
    vector<libImport_Worker *> workers;
    pthread_attr_t *worker_attr;
    pthread_mutex_t *queue_mutex;
    queue<string> queue_file;
};

#endif // _LIB_IMPORT_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
