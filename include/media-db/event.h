#ifndef _LIB_EVENT_H
#define _LIB_EVENT_H

#include <event2/event.h>

#ifndef _libEvent_STACK_SIZE
#define _libEvent_STACK_SIZE    (32768 * 2)
#endif

class libEvent
{
public:
    libEvent();
    virtual ~libEvent();
};

typedef deque<libEvent *> libEvent_Deque;

class libEvent_Client
{
public:
    libEvent_Client();
    virtual ~libEvent_Client();

    void EventPush(libEvent *event);

protected:
    libEvent_Deque event_queue;
    pthread_mutex_t event_queue_mutex;
    pthread_cond_t event_condition;
    pthread_mutex_t event_condition_mutex;
};

#define gEventServer    libEvent_Server::GetInstance()
#define gEventServerPtr libEvent_Server::GetInstancePtr()

typedef map<libEvent_Client *, libEvent_Client *> libEvent_ClientMap;

class libEvent_Server : public libThread, public libSingleton <libEvent_Server>
{
public:
    libEvent_Server();
    virtual ~libEvent_Server();

    virtual void *Entry(void);

    void Register(libEvent_Client *client);
    void Unregister(libEvent_Client *client);

    static void Start(void);

    void Dispatch(void);

protected:
    libEvent_ClientMap clients;
    pthread_mutex_t client_map_mutex;
    struct event_base *eb;
};

#endif // _LIB_EVENT_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
