#ifndef __LIB_MAIN_H
#define __LIB_MAIN_H

#define _MDB_MAX_OPTIONS    256

#ifndef _libMain_SIGNAL_THREAD_STACK_SIZE
#define _libMain_SIGNAL_THREAD_STACK_SIZE    (32768 * 2)
#endif

typedef struct __libMainOption {
    string long_opt;
    int has_arg;
    string arg_name;
    char short_opt;
    string help;
    string default_value;
} libMainOption;

class libMain_SignalHandler : public libThread
{
public:
    libMain_SignalHandler(void)
        : libThread(_libMain_SIGNAL_THREAD_STACK_SIZE) { }
    virtual ~libMain_SignalHandler() { Join(); }

    virtual void *Entry(void);

    static void Start(void);
};

class libMain : public libSingleton <libMain>
{
public:
    libMain(int argc, char * const *argv);

    virtual ~libMain();

    void ParseArguments(void);
    void Usage(void);
    void Daemonize(void);

    virtual int Exec(void) = 0;

protected:
    int argc;
    char * const *argv;
    string prog_name;
    string prog_usage;
    string prog_help;
    vector<libMainOption *> options;
    bool verbose;
    bool debug;

    virtual void SetOption(char option, const char *optarg);
};

#endif // __LIB_MAIN_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
