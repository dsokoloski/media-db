#ifndef _LIB_SOCKET_H
#define _LIB_SOCKET_H

#define _LIB_SOCKET_PATH_UNIX   "/proc/net/unix"

using namespace std;

class libSocketLocal;
class libSocketRemote;
class libSocketClient;
class libSocketServer;

class libSocket
{
public:
    libSocket();
    libSocket(const string &node);
    libSocket(const string &node, const string &service);
    virtual ~libSocket();

    ssize_t Read(uint8_t *buffer, ssize_t length);
    ssize_t Write(uint8_t *buffer, ssize_t length);

    ssize_t BlockingRead(uint8_t *buffer, ssize_t length);
    ssize_t BlockingWrite(uint8_t *buffer, ssize_t length);

protected:
    friend class libSocketLocal;
    friend class libSocketRemote;
    friend class libSocketClient;
    friend class libSocketServer;

    void Create(void);

    int sd;
    int family;
    sockaddr *sa;
    socklen_t sa_size;
    string node;
    string service;

    enum libSocketType
    {
        LST_NULL,
        LST_CLIENT,
        LST_SERVER
    };

    libSocketType type;

    enum libSocketState
    {
        LSS_INIT,
        LSS_CONNECTED,
        LSS_ACCEPTED,
        LSS_CLOSED,
    };

    libSocketState state;

    uint64_t bytes_in;
    uint64_t bytes_out;
};

class libSocketLocal
{
protected:
    libSocketLocal(libSocket *base, const string &node);
    virtual ~libSocketLocal();

    int IsValid(void);

    libSocket *base;
};

class libSocketRemote
{
protected:
    libSocketRemote(libSocket *base, const string &node, const string &service);
    virtual ~libSocketRemote();

    libSocket *base;
};

class libSocketClient
{
protected:
    libSocketClient(libSocket *base);
    virtual ~libSocketClient();

    libSocket *base;
};

class libSocketServer
{
public:
    libSocket *Accept(void);

protected:
    libSocketServer(libSocket *base);
    virtual ~libSocketServer();

    libSocket *base;
};

class libSocketClientLocal
    : public libSocket, public libSocketClient, protected libSocketLocal
{
public:
    libSocketClientLocal(const string &node);
    virtual ~libSocketClientLocal();

protected:
};

class libSocketServerLocal
    : public libSocket, public libSocketServer, protected libSocketLocal
{
public:
    libSocketServerLocal(const string &node);
    virtual ~libSocketServerLocal();

protected:
};

class libSocketClientRemote
    : public libSocket, public libSocketClient, protected libSocketRemote
{
public:
    libSocketClientRemote(const string &node, const string &service);
    virtual ~libSocketClientRemote();

protected:
};

class libSocketServerRemote
    : public libSocket, public libSocketServer, protected libSocketRemote
{
public:
    libSocketServerRemote(const string &node, const string &service);
    virtual ~libSocketServerRemote();

protected:
};

class libGetAddrInfoException : public runtime_error
{
public:
    explicit libGetAddrInfoException(
        const string &where_arg, const string &what_arg, int why_arg) throw();
    virtual ~libGetAddrInfoException() throw();

    virtual const char *what() const throw();

    string where_arg;
    string what_arg;
    int why_arg;
    const char *message;
};

#endif // _LIB_SOCKET_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
