#ifndef _LIB_EXPAT_XML_H
#define _LIB_EXPAT_XML_H

class ExpatXmlTag
{
public:
    ExpatXmlTag(const char *name, const char **attr);

    inline string GetName(void) const { return name; };
    bool ParamExists(const string &key);
    string GetParamValue(const string &key);
    inline string GetText(void) const { return text; };
    inline void SetText(const string &text) { this->text = text; };
    void *GetData(void) { return data; };
    inline void SetData(void *data) { this->data = data; };

    bool operator==(const char *tag);
    bool operator!=(const char *tag);

protected:
    typedef map<string, string> ParamMap;
    ParamMap param;

    string name;
    string text;
    void *data;
};

class ExpatXmlParser
{
public:
    ExpatXmlParser(void);
    virtual ~ExpatXmlParser();

    virtual void Reset(void);
    void SetPrivateData(void *priv_data) { this->priv_data = priv_data; }
    virtual void Parse(const string &chunk);

    void ParseError(const string &what);

    virtual void ParseElementOpen(ExpatXmlTag *tag) = 0;
    virtual void ParseElementClose(ExpatXmlTag *tag) = 0;

    XML_Parser p;
    int done;

    typedef vector<ExpatXmlTag *> TagStack;
    TagStack stack;

protected:
    void *priv_data;
};

class ExpatXmlParseException : public runtime_error
{
public:
    explicit ExpatXmlParseException(const string &what,
        int row, int col)
        : runtime_error(what), row(row), col(col)
        { };
    virtual ~ExpatXmlParseException() throw() { };

    int row;
    int col;
};

class ExpatXmlKeyNotFound : public runtime_error
{
public:
    explicit ExpatXmlKeyNotFound(const string &key)
        : runtime_error(key) { };
    virtual ~ExpatXmlKeyNotFound() throw() { };
};

#endif // _LIB_EXPAT_XML_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
