#ifndef _LIB_THREAD_H
#define _LIB_THREAD_H

#ifndef _libThread_STACK_SIZE
#define _libThread_STACK_SIZE   (32768 * 2)
#endif

class libThread
{
public:
    libThread(int stack_size = _libThread_STACK_SIZE);
    virtual ~libThread();

    bool IsValid(void) { return (id != 0); }

    void SetName(const string &name);
    void SetAffinity(int cpu);

    pthread_t GetId(void) { return id; }

    void Create(void);
    virtual void *Entry(void) = 0;
    void Join(void);

protected:
    pthread_t id;
    pthread_attr_t attr;
    string name;
};

#endif // _LIB_THREAD_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
