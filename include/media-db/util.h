#ifndef _LIB_UTIL_H
#define _LIB_UTIL_H

#define ISDOT(a)    (a[0] == '.' && (!a[1] || (a[1] == '.' && !a[2])))

#define O_INFO  libOutput_INFO
#define O_WARN  libOutput_WARN
#define O_ERROR libOutput_ERROR
#define O_DEBUG libOutput_DEBUG

enum libOutputType
{
    libOutput_STDERR,
    libOutput_SYSLOG,
};

enum libOutputLevel
{
    libOutput_INFO,
    libOutput_WARN,
    libOutput_ERROR,
    libOutput_DEBUG,
};

typedef struct __libOutputContext
{
    libOutputType type;
    pthread_mutex_t *mutex;
    int syslog_facility;
    int syslog_options;
    string syslog_ident;
    bool debug;
} libOutputContext;

void libOutputInit(libOutputContext &ctx);
void libOutputFree(void);

void libOutput(libOutputLevel level, const char *format, ...);

template <typename T> class libSingleton
{
    static T* instance;

public:
    libSingleton(void)
    {
        assert(!instance);
        instance = (T*)this;
    }

    ~libSingleton(void)
    {
        assert(instance);
        instance = NULL;
    }

    static T& GetInstance(void)
    {
        assert(instance);
        return (*instance);
    }

    static T* GetInstancePtr(void)
    {
        assert(instance);
        return (instance);
    }
};

template <typename T> T* libSingleton <T>::instance = 0;

void libUtil_bin2hex(const uint8_t *bin, string &hex, size_t length);

long libUtil_getcpus(void);
long libUtil_getpagesize(void);

#endif // _LIB_UTIL_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
