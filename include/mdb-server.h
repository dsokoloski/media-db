#ifndef __MDB_SERVER_H
#define __MDB_SERVER_H

class mdbServer : public libMain, public libEvent_Client
{
public:
    mdbServer(int argc, char * const *argv);

    virtual ~mdbServer();

    int Exec(void);
};

#endif // __MDB_SERVER_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
