dnl adl_GET_YEAR(RESULT)
dnl =================================
dnl Return the current 4-digit year.
AC_DEFUN([adl_GET_YEAR], [ $1=`(date '+%Y')`])
