# Media DB

AC_PREREQ([2.63])
AC_INIT([Media/DB], [1.0], [http://sokoloski.ca/], [media-db], [http://sokoloski.ca/])
AC_CONFIG_AUX_DIR([.])
AM_INIT_AUTOMAKE
AC_CONFIG_SRCDIR([src])
AC_CONFIG_SUBDIRS([file uriparser])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])
LT_INIT
AC_SUBST([LIBTOOL_DEPS])
#m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])

# Checks for programs.
AC_PROG_CXX
AC_PROG_INSTALL

# Set language.
AC_LANG([C++])

# Set year for copyright message.
adl_GET_YEAR(current_year)
AC_DEFINE_UNQUOTED([COPYRIGHT_YEAR], ["$current_year"], [Copyright year.])

# Checks for libraries.
AC_CHECK_LIB([z], [inflate], [], [
	AC_MSG_ERROR([libz not found but is required.])])
AC_CHECK_LIB([pthread], [pthread_create], [], [
	AC_MSG_ERROR([libpthread not found but is required.])])
AC_CHECK_LIB([crypto], [SHA1], [], [
	AC_MSG_ERROR([libcrypto not found but is required.])])
AC_CHECK_LIB([sqlite3], [sqlite3_open], [], [
	AC_MSG_ERROR([libsqlite3 not found but is required.])])
AC_CHECK_LIB([expat], [XML_ParserCreate], [], [
	AC_MSG_ERROR([libexpat is not found or unusable.])])
AC_CHECK_LIB([event_core], [event_config_new], [], [
	AC_MSG_ERROR([libevent2 is not found or unusable.])])

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_TYPE_SIZE_T

# Checks for library functions.

# Check with/out enable/disable arguments
AC_ARG_ENABLE([debug], AS_HELP_STRING([--enable-debug], [enable debug support]))

AS_IF([test "x$enable_debug" != "xyes"], [
    AC_DEFINE([NDEBUG], [1], [Enable debug support.])
])

adl_RECURSIVE_EVAL("${datadir}", datadir)

AC_ARG_WITH([magic-db],
	AS_HELP_STRING([--with-magic-db],
		[specify a default magic database file]),
	AC_DEFINE_UNQUOTED([DEFAULT_MAGIC_DB], ["$withval"],
		[Default magic database file.]),
	AC_DEFINE_UNQUOTED([DEFAULT_MAGIC_DB], ["$datadir/misc/magic.mgc"],
		[Default magic database file.])
)

AC_ARG_WITH([mime-file],
	AS_HELP_STRING([--with-mime-file],
		[specify a default MIME file]),
	AC_DEFINE_UNQUOTED([DEFAULT_MIME_FILE], ["$withval"],
		[Default MIME file.]),
	AC_DEFINE([DEFAULT_MIME_FILE], ["/etc/mime.types"],
		[Default MIME file.])
)

# Output files
AC_CONFIG_FILES([Makefile src/Makefile media-db.spec media-db.conf libmedia-db.pc])
AC_OUTPUT
